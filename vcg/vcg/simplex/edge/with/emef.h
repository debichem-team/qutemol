#ifndef __VCGLIB_EDGE_EM_TYPE
#define __VCGLIB_EDGE_EM_TYPE

#define EDGE_TYPE EdgeEMEF 
#define __VCGLIB_EDGE_EM
#define __VCGLIB_EDGE_EF

#include <vcg/simplex/edge/base.h> 

#undef EDGE_TYPE 
#undef __VCGLIB_EDGE_EF
#undef __VCGLIB_EDGE_EM

#endif
