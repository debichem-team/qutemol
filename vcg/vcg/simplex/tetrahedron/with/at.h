#ifndef __VCGLIB_TETRA_AT_TYPE
#define __VCGLIB_TETRA_AT_TYPE

#define TETRA_TYPE TetraAT

#define __VCGLIB_TETRA_AT

#include <vcg/simplex/tetrahedron/base.h> 

#undef TETRA_TYPE 

#undef __VCGLIB_TETRA_AT

#endif
