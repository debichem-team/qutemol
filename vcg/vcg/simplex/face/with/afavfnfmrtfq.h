#ifndef __VCGLIB_FACE_AFAVFNFMRTFQ_TYPE
#define __VCGLIB_FACE_AFAVFNFMRTFQ_TYPE

#define FACE_TYPE FaceAFAVFNFMRTFQ

#define __VCGLIB_FACE_AF
#define __VCGLIB_FACE_FN
#define __VCGLIB_FACE_AV
#define __VCGLIB_FACE_FM
#define __VCGLIB_FACE_RT
#define __VCGLIB_FACE_FQ

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_FN
#undef __VCGLIB_FACE_AV
#undef __VCGLIB_FACE_FM
#undef __VCGLIB_FACE_RT
#undef __VCGLIB_FACE_FQ
#endif
