#ifndef __VCGLIB_FACE_AFAVFNFMRT_TYPE
#define __VCGLIB_FACE_AFAVFNFMRT_TYPE

#define FACE_TYPE FaceAFFNFMRT

#define __VCGLIB_FACE_AF
#define __VCGLIB_FACE_FN
#define __VCGLIB_FACE_FM
#define __VCGLIB_FACE_RT

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_FN
#undef __VCGLIB_FACE_FM
#undef __VCGLIB_FACE_RT
#endif